package com.prodcons;

import java.util.ArrayList;

public class Main {

    public static void main(String[] args) {
	    WorkQueue workQueue = new WorkQueue();

	    ProducerThread producerThread1 = new ProducerThread(1, workQueue);
	    ConsumerThread consumerThread1 = new ConsumerThread(1, workQueue);
	    producerThread1.start();
	    consumerThread1.start();

		/*ProducerThread producerThread2 = new ProducerThread(2, workQueue);
		ConsumerThread consumerThread2 = new ConsumerThread(2, workQueue);
		producerThread2.start();
		consumerThread2.start();*/

    }
}
