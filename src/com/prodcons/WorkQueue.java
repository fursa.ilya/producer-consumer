package com.prodcons;

import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.Semaphore;

public class WorkQueue {
    private CopyOnWriteArrayList<Integer> list = new CopyOnWriteArrayList<>();
    private Semaphore consumerSemaphore = new Semaphore(0);
    private Semaphore producerSemaphore = new Semaphore(1);

    public void consumeItem(int threadNumber, int pos) throws InterruptedException {
        consumerSemaphore.acquire();
        System.out.println("Consumer №" + threadNumber + " consumed data: " + list.get(pos));
        producerSemaphore.release();
    }

    public void produceItem(int threadNumber, int data, int pos) throws InterruptedException {
        producerSemaphore.acquire();
        list.add(pos, data);
        System.out.println("Producer №" + threadNumber + " produced data: " + list.get(pos));
        consumerSemaphore.release();
    }
 }
