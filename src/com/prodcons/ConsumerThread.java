package com.prodcons;

public class ConsumerThread extends Thread {
    private WorkQueue workQueue;
    private int threadNumber;

    public ConsumerThread(int threadNumber, WorkQueue workQueue) {
        this.workQueue = workQueue;
        this.threadNumber = threadNumber;
    }

    @Override
    public void run() {
        for (int i = 0; i < 5; i++) {
            try {
                workQueue.consumeItem(threadNumber, i);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
