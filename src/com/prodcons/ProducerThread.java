package com.prodcons;

public class ProducerThread extends Thread {
    private WorkQueue workQueue;
    private int threadNumber;

    public ProducerThread(int threadNumber, WorkQueue workQueue) {
        this.threadNumber = threadNumber;
        this.workQueue = workQueue;
    }

    @Override
    public void run() {
        for (int i = 0; i < 5; i++) {
            try {
                workQueue.produceItem(threadNumber,i + 1, i);
                if(i % 2 == 0) {
                    Thread.sleep(5000);
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
